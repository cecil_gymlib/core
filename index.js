import colors from './ui/colors'
import headline from './ui/headline'
import paragraph from './ui/paragraph'
import {smallScreen, encode, decode} from './outils/utils'

module.exports [
  colors, headline, paragraph, smallScreen, encode, decode
]