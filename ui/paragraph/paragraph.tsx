import React, { FunctionComponent } from 'react';
import { Text, TextProps } from 'react-native';
import {Colors as colors} from '@cecil_arthur/uikit.colors';

export interface ParagraphProps extends TextProps {
  size?: 1 | 2 | 3 | 4;
  color?: string;
  bold?: boolean;
  semibold?: boolean;
  oneline?: boolean;
  italic?: boolean;
  italicbold?: boolean;
  cursive?: boolean;
  cursivebold?: boolean;
}

const stylesMapping = {
  1: (oneline: boolean) => ({
    fontSize: 18,
    lineHeight: oneline ? 22 : 26,
  }),
  2: (oneline: boolean) => ({
    fontSize: 16,
    lineHeight: oneline ? 20 : 22,
  }),
  3: (oneline: boolean) => ({
    fontSize: 14,
    lineHeight: oneline ? 16 : 18,
  }),
  4: (oneline: boolean) => ({
    fontSize: 12,
    lineHeight: oneline ? 14 : 16,
  }),
};

export const Paragraph: FunctionComponent<ParagraphProps> = ({
  size = 1,
  color = colors.shades.black,
  bold = false,
  semibold = false,
  oneline = false,
  italic = false,
  cursive = false,
  italicbold = false,
  cursivebold = false,
  style,
  children,
  ...textProps
}) => (
  <Text
    style={[
      {
        color,
        ...stylesMapping[size](oneline),
        fontFamily: cursive
          ? 'GTHaptikRegularRotalic'
          : cursivebold
          ? 'GTHaptikBoldRotalic'
          : italic
          ? 'GTHaptikRegular-Oblique'
          : italicbold
          ? 'GTHaptikBold-Oblique'
          : bold
          ? 'GTHaptikBold'
          : semibold
          ? 'GTHaptikMedium'
          : 'GTHaptikRegular',
      },
      style,
    ]}
    {...textProps}
  >
    {children}
  </Text>
);

export default Paragraph;
