import React, { FunctionComponent, PureComponent } from 'react';
import { Text, TextProps, StyleProp, TextStyle } from 'react-native';
import {Colors as colors} from '@cecil_arthur/uikit.colors';

export interface HeadlineProps extends TextProps {
  color?: string;
  semibold?: boolean;
  bold?: boolean;
  italic?: boolean;
  cursive?: boolean;
  size?: 1 | 2 | 3;
}

const BASE_HEADLINE_STYLES: TextStyle = {
  fontFamily: 'GTHaptikRegular',
  color: colors.shades.black,
};

const H1_STYLES: TextStyle = {
  fontFamily: 'GTHaptikBold',
  fontSize: 28,
  lineHeight: 32,
};

const H2_STYLES: TextStyle = {
  fontSize: 20,
  lineHeight: 28,
};

const H3_STYLES: TextStyle = {
  fontSize: 18,
  lineHeight: 24,
  fontFamily: 'GTHaptikMedium',
};

const VARIANTS_STYLES: { [key: string]: TextStyle } = {
  semibold: {
    fontFamily: 'GTHaptikMedium',
  },
  bold: {
    fontFamily: 'GTHaptikBold',
  },
  italic: {
    fontFamily: 'GTHaptikRegular-Oblique',
  },
  semiboldCursive: {
    fontFamily: 'GTHaptikMediumRotalic',
  },
  cursive: {
    fontFamily: 'GTHaptikRegularRotalic',
  },
  boldCursive: {
    fontFamily: 'GTHaptikBoldRotalic',
  },
};

export class Headline extends PureComponent<HeadlineProps> {
  render() {
    const {
      children,
      style,
      size,
      semibold,
      bold,
      italic,
      cursive,
      color,
      ...textProps
    } = this.props;
    const styles: StyleProp<TextStyle> = [BASE_HEADLINE_STYLES];

    (!size || size === 1) && styles.push(H1_STYLES);
    size === 2 && styles.push(H2_STYLES);
    size === 3 && styles.push(H3_STYLES);
    semibold && styles.push(VARIANTS_STYLES.semibold);
    bold && styles.push(VARIANTS_STYLES.bold);
    italic && styles.push(VARIANTS_STYLES.italic);
    cursive && styles.push(VARIANTS_STYLES.cursive);
    semibold && cursive && styles.push(VARIANTS_STYLES.semiboldCursive);
    bold && cursive && styles.push(VARIANTS_STYLES.boldCursive);
    color && styles.push({ color });
    style && styles.push(style);
    return (
      <Text style={styles} {...textProps}>
        {children}
      </Text>
    );
  }
}

