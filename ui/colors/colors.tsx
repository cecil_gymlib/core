export const Colors = {
  primary: '#3C0CBF',
  complementary: {
    lila: '#E8E0FC',
    peach: '#F9DBDA',
    pearl: '#D3E8EA',
    lemon: '#F7F7DA',
    prelude: '#BEBAE8',
    daintree: '#02233B',
  },
  shades: {
    black: '#1B1B1D',
    darkGrey: '#525268',
    midGrey: '#C1C1C4',
    lightGrey: '#F1F1F3',
    white: '#FFFFFF',
  },
  status: {
    ok: {
      light: '#EDF9DD',
      regular: '#48D768',
      dark: '#198932',
    },
    info: '#F2C94C',
    warning: '#FA9943',
    error: '#E43B50',
  },
  subscriptions: {
    standard: {
      major: '#008BC6',
      minor: '#D9F2EC',
    },
    advanced: {
      major: '#C88520',
      minor: '#F2E9D9',
    },
    premium: {
      major: '#1B1B1D',
      minor: '#C1C1C3',
    },
  },
  rainbow: {
    green: 'green'
  }
};



export const ColorCard = ({ color, label }) => (
  <div style={{ margin: '1rem' }}>
    <div
      style={{
        width: '200px',
        height: '200px',
        borderRadius: '4px',
        backgroundColor: color,
        boxShadow: '0px 1px 4px rgba(190, 186, 232, 0.5)',
      }}
    />
    <label style={{ marginTop: '1rem', display: 'block' }}>{label}</label>
    <span style={{ fontSize: '.8rem' }}>{color}</span>
  </div>
);

export const ColorCardContainer = ({ children }) => (
  <article
    style={{
      display: 'flex',
      flexWrap: 'wrap',
    }}
  >
    {children}
  </article>
);
