import {Dimensions} from  'react-native';
import { Buffer } from 'buffer';

export function smallScreen(): boolean {
  return Dimensions.get('screen').width <= 375;
}


export const encode = (input: string) => {
  return Buffer.from(input).toString('base64');
};

export const decode = (input: string) => {
  return Buffer.from(input, 'base64').toString('binary');
};
